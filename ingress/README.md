## Deploy

`kubectl apply -f ingress-nginx/`

`kubectl apply -f custom-ingress-rules.yml`

## Cleaning ALL

`kubectl delete deployment nginx-ingress-controller`

`kubectl delete service nginx-ingress`

`kubectl delete clusterrole nginx-ingress-clusterrole`

`kubectl delete serviceaccount nginx-ingress-serviceaccount`

`kubectl delete role nginx-ingress-role`

`kubectl delete rolebinding nginx-ingress-role-nisa-binding`

`kubectl delete clusterrolebinding nginx-ingress-clusterrole-nisa-binding`

`kubectl delete service default-http-backend`

`kubectl delete deploy default-http-backend`
