#!/bin/bash

worker=("k8-hardw-node1" "k8-hardw-node2")
controler=k8-hardw-master1

echo "CA configuration file"
cat > ca-config.json <<EOF
{
  "signing": {
    "default": {
      "expiry": "8760h"
    },
    "profiles": {
      "kubernetes": {
        "usages": ["signing", "key encipherment", "server auth", "client auth"],
        "expiry": "8760h"
      }
    }
  }
}
EOF

echo "CA certificate signing request"
cat > ca-csr.json <<EOF
{
  "CN": "Kubernetes",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "Kubernetes",
      "OU": "CA",
      "ST": "Oregon"
    }
  ]
}
EOF

echo "Generate the CA certificate and private key"
cfssl gencert -initca ca-csr.json | cfssljson -bare ca


echo "Create the admin client certificate signing request"
cat > admin-csr.json <<EOF
{
  "CN": "admin",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:masters",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

echo "Generate the admin client certificate and private key"
cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -profile=kubernetes \
  admin-csr.json | cfssljson -bare admin


echo "Generate a certificate and private key for each Kubernetes worker node"
for instance in "${worker[@]}"
do
  cat > ${instance}-csr.json <<EOF
{
  "CN": "system:node:${instance}",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:nodes",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF
done

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -hostname=k8-hardw-node1,10.20.72.166 \
  -profile=kubernetes \
  ${instance}-csr.json | cfssljson -bare k8-hardw-node1

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -hostname=k8-hardw-node2,10.20.72.165 \
  -profile=kubernetes \
  ${instance}-csr.json | cfssljson -bare k8-hardw-node2


echo "Create the kube-proxy client certificate signing request"
cat > kube-proxy-csr.json <<EOF
{
  "CN": "system:kube-proxy",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:node-proxier",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -profile=kubernetes \
  kube-proxy-csr.json | cfssljson -bare kube-proxy


echo "Create the Kubernetes API Server certificate signing request"
cat > kubernetes-csr.json <<EOF
{
  "CN": "kubernetes",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "Kubernetes",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -hostname=10.32.0.1,10.20.72.165,10.20.72.166,10.20.72.167,127.0.0.1,kubernetes.default \
  -profile=kubernetes \
  kubernetes-csr.json | cfssljson -bare kubernetes

echo "Copy the appropriate certificates and private keys to each worker instance"
for instance in "${worker[@]}"
do
  scp ca.pem ${instance}.local:~/pki/
  scp ${instance}-key.pem ${instance}.local:~/pki/
  scp ${instance}.pem ${instance}.local:~/pki/
done  

echo "Copy the appropriate certificates and private keys to each controller instance"


scp ca.pem ${controler}.local:~/pki/
scp ca-key.pem ${controler}.local:~/pki/
scp kubernetes-key.pem ${controler}.local:~/pki/
scp kubernetes.pem ${controler}.local:~/pki/


ENCRYPTION_KEY=$(head -c 32 /dev/urandom | base64)
echo "Create the encryption-config.yaml encryption config file"
cat > encryption-config.yaml <<EOF
kind: EncryptionConfig
apiVersion: v1
resources:
  - resources:
      - secrets
    providers:
      - aescbc:
          keys:
            - name: key1
              secret: ${ENCRYPTION_KEY}
      - identity: {}
EOF

for instance in "${worker[@]}"
do
  scp encryption-config.yaml ${instance}.local:~/pki/
done  
