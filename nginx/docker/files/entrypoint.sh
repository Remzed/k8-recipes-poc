#!/bin/bash

echo "
upstream ${var1} {
    server unix:${var2}/tmp/sockets/unicorn.mep.socket fail_timeout=0;
}

server {
    listen 127.0.0.1:8080;
    server_name ${var1}.url.tld;

    # root ${var2}/public;
    access_log /var/log/nginx/mep_access.log;
    error_log /var/log/nginx/mep_error.log;
    rewrite_log on;

    location /basic_status {
        stub_status;
    }

    location ^~ /app/ {
        alias   ${var2}/mobile/;
    }

    location ^~ /cast/ {
        alias   ${var2}/mhr-cast/;
    }

    location = /app {
        rewrite ^ app/ permanent;
    }

    location = / {
        rewrite ^ app/ permanent;
    }

    location = /cms {
        port_in_redirect off;
        rewrite ^ /trainer/trainer_applications permanent;
    }


    # Add expires header for static content
    location ~* \.(js|css|apk|jpg|jpeg|gif|png|swf|html|mp4|pdf|ipa|ttf|plist|ipa|xls|xlsx|zip|woff|woff2)$ {
      if (-f \$request_filename) {
          add_header Access-Control-Allow-Origin *;
          expires      max;
          break;
      }

      if (!-f \$request_filename) {
          proxy_pass http://${var1};
          break;
      }
    }

    location / {
        #all requests are sent to the UNIX socket
        proxy_pass  http://${var1};
        proxy_redirect     off;

        proxy_set_header   Host             \$http_host;
        proxy_set_header   X-Real-IP        \$remote_addr;
        proxy_set_header   X-Forwarded-For  \$proxy_add_x_forwarded_for;

        client_max_body_size       1000m;
        client_body_buffer_size    128k;

        proxy_connect_timeout      90;
        proxy_send_timeout         90;
        proxy_read_timeout         90;

        proxy_buffer_size          4k;
        proxy_buffers              4 32k;
        proxy_busy_buffers_size    64k;
        proxy_temp_file_write_size 64k;
    }
}
" >> /etc/nginx/conf.d/instance.conf

cat /etc/nginx/conf.d/instance.conf

/usr/sbin/nginx -g "daemon off;" #worker_processes ${worker_processes};"
