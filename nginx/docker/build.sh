#!/bin/bash

DOCKER_IMAGE="nginx"

docker build -t "${DOCKER_IMAGE}" .

docker rm -f "${DOCKER_IMAGE}"

docker run --name "${DOCKER_IMAGE}" "${DOCKER_IMAGE}"

