#!/bin/bash

if [ $(id -u) -ne 0 ]; then
	echo "Run this command with sudo ";
	exit 1;
fi

echo "Update sources"
apt update
echo "Install apt-transport-https and docker"
apt install -y apt-transport-https docker.io

echo "Add repository.list"
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" >> /etc/apt/sources.list.d/kubernetes.list

echo "Add apt-key"
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -

echo "Update sources"
apt update

echo "Install kubernetes tools: kubelet, kubeadm, kubernetes-cni"
apt install -y kubelet kubeadm kubernetes-cni

SWAP=$(lsblk -l|grep SWAP|cut -d' ' -f1)
echo "Disable swap"
swapoff /dev/$SWAP
echo "Disabled swap on fstab"
sed -i '$ s/UUID/#UUID/g' fstab
